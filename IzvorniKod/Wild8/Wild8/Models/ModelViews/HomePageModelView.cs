﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wild8.Models.ModelViews
{
    public class HomePageModelView
    {
        public Meal FirstMeal { get; set; }
        public Meal SecondMeal { get; set; }
        public Meal ThirdMeal { get; set; }
    }
}