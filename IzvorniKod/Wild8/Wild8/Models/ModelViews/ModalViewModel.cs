﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wild8.Models.ModelViews
{
    public class ModalViewModel
    {
        public string Title { get; set; }
        public string Type { get; set; }
    }
}